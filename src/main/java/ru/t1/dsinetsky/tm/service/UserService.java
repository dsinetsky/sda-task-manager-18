package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.repository.IUserRepository;
import ru.t1.dsinetsky.tm.api.service.IUserService;
import ru.t1.dsinetsky.tm.builder.repository.UserBuilder;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.user.*;
import ru.t1.dsinetsky.tm.model.User;
import ru.t1.dsinetsky.tm.util.HashUtil;

import java.util.List;

public final class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(final String login, final String password) throws GeneralUserException {
        final User newUser = UserBuilder.create()
                .login(login)
                .password(password)
                .toUser();
        create(newUser);
        return newUser;
    }

    @Override
    public User create(final String login, final String password, final String firstName, final String lastName, final String middleName) throws GeneralUserException {
        final User newUser = UserBuilder.create()
                .login(login)
                .password(password)
                .firstName(firstName)
                .lastName(lastName)
                .middleName(middleName)
                .toUser();
        create(newUser);
        return newUser;
    }

    @Override
    public User create(final String login, final String password, final String email) throws GeneralUserException {
        final User newUser = UserBuilder.create()
                .login(login)
                .password(password)
                .email(email)
                .toUser();
        create(newUser);
        return newUser;
    }

    @Override
    public User create(final String login, final String password, final String email, final String firstName, final String lastName, final String middleName) throws GeneralUserException {
        final User newUser = UserBuilder.create()
                .login(login)
                .password(password)
                .email(email)
                .firstName(firstName)
                .lastName(lastName)
                .middleName(middleName)
                .toUser();
        create(newUser);
        return newUser;
    }

    @Override
    public User create(final String login, final String password, final Role role, final String email, final String firstName, final String lastName, final String middleName) throws GeneralUserException {
        final User newUser = UserBuilder.create()
                .login(login)
                .password(password)
                .role(role)
                .email(email)
                .firstName(firstName)
                .lastName(lastName)
                .middleName(middleName)
                .toUser();
        create(newUser);
        return newUser;
    }

    @Override
    public User create(final String login, final String password, final Role role) throws GeneralUserException {
        final User newUser = UserBuilder.create()
                .login(login)
                .password(password)
                .role(role)
                .toUser();
        create(newUser);
        return newUser;
    }

    @Override
    public User create(final User newUser) throws GeneralUserException {
        if(newUser == null) throw new InvalidUserException();
        final String login = newUser.getLogin();
        if(login == null || login.isEmpty()) throw new IncorrectLoginPasswordException();
        if(isUserExistByLogin(login)) throw new UserAlreadyExistException();
        final String password = newUser.getPasswordHash();
        if(password == null || password.isEmpty()) throw new IncorrectLoginPasswordException();
        final Role role = newUser.getRole();
        if(role == null) throw new RoleIsEmptyException();
        newUser.setPasswordHash(HashUtil.salt(password));
        userRepository.add(newUser);
        return newUser;
    }

    @Override
    public void removeUserById(String id) throws GeneralUserException {
        if(id == null || id.isEmpty()) throw new UserIdIsEmptyException();
        if(!isUserExistById(id)) throw new UserNotFoundException();
        userRepository.removeById(id);
    }

    @Override
    public void removeUserByLogin(String login) throws GeneralUserException {
        if(login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if(!isUserExistByLogin(login)) throw new UserNotFoundException();
        userRepository.removeByLogin(login);
    }

    @Override
    public User findUserById(String id) throws GeneralUserException {
        if(id == null || id.isEmpty()) throw new UserIdIsEmptyException();
        if(!isUserExistById(id)) throw new UserNotFoundException();
        return userRepository.findUserById(id);
    }

    @Override
    public User findUserByLogin(String login) throws GeneralUserException {
        if(login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if(!isUserExistByLogin(login)) throw new UserNotFoundException();
        return userRepository.findUserByLogin(login);
    }

    @Override
    public User updateUserById(String id, String firstName, String lastName, String middleName) throws GeneralUserException {
        if(id == null || id.isEmpty()) throw new UserIdIsEmptyException();
        if(!isUserExistById(id)) throw new UserNotFoundException();
        final User user = findUserById(id);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

    @Override
    public User updateUserByLogin(String login, String firstName, String lastName, String middleName) throws GeneralUserException {
        if(login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if(!isUserExistByLogin(login)) throw new UserNotFoundException();
        final User user = findUserByLogin(login);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

    @Override
    public User updateEmailById(String id, String email) throws GeneralUserException {
        if(id == null || id.isEmpty()) throw new UserIdIsEmptyException();
        if(!isUserExistById(id)) throw new UserNotFoundException();
        final User user = findUserById(id);
        user.setEmail(email);
        return user;
    }

    @Override
    public User updateEmailByLogin(String login, String email) throws GeneralUserException {
        if(login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if(!isUserExistByLogin(login)) throw new UserNotFoundException();
        final User user = findUserByLogin(login);
        user.setEmail(email);
        return user;
    }

    @Override
    public User changePassword(String id, String password) throws GeneralUserException {
        if(id == null || id.isEmpty()) throw new UserIdIsEmptyException();
        if(!isUserExistById(id)) throw new UserNotFoundException();
        final User user = findUserById(id);
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public List<User> returnAll() {
        return userRepository.returnAll();
    }

    @Override
    public boolean isUserExistById(String id) throws GeneralUserException {
        if(id == null || id.isEmpty()) throw new UserIdIsEmptyException();
        return userRepository.isUserIdExist(id);
    }

    @Override
    public boolean isUserExistByLogin(String login) throws GeneralUserException {
        if(login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        return userRepository.isUserLoginExist(login);
    }

}
