package ru.t1.dsinetsky.tm.command.system;

import ru.t1.dsinetsky.tm.api.service.ICommandService;
import ru.t1.dsinetsky.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

}
