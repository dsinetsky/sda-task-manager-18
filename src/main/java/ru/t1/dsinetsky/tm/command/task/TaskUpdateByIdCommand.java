package ru.t1.dsinetsky.tm.command.task;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    public static final String NAME = TerminalConst.CMD_UPD_TASK_BY_ID;

    public static final String DESCRIPTION = "Updates name and description of task (if any) found by id";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter id of task:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter new name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter new description:");
        final String description = TerminalUtil.nextLine();
        final Task task = getTaskService().updateById(id, name, description);
        showTask(task);
        System.out.println("Task successfully updated!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
