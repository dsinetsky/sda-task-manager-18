package ru.t1.dsinetsky.tm.command.task;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    public static final String NAME = TerminalConst.CMD_UNBIND_TASK_TO_PROJECT;

    public static final String DESCRIPTION = "Unbinds task to project(if any)";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter Id of project:");
        final String projectEnter = TerminalUtil.nextLine();
        System.out.println("Enter Id of task:");
        final String taskEnter = TerminalUtil.nextLine();
        getProjectTaskService().unbindProjectById(projectEnter, taskEnter);
        System.out.println("Task successfully unbound to project!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
