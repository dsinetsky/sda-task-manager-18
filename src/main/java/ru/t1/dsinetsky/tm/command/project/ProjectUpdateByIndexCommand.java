package ru.t1.dsinetsky.tm.command.project;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = TerminalConst.CMD_UPD_PROJECT_BY_INDEX;

    public static final String DESCRIPTION = "Updates name and description of project (if any) found by index";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter index of project:");
        final int index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter new name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter new description:");
        final String description = TerminalUtil.nextLine();
        final Project project = getProjectService().updateByIndex(index, name, description);
        showProject(project);
        System.out.println("Project successfully updated!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
