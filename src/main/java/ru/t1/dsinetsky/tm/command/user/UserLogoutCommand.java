package ru.t1.dsinetsky.tm.command.user;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.User;

public final class UserLogoutCommand extends AbstractUserCommand{

    public static final String NAME = TerminalConst.CMD_LOGOUT_USER;

    public static final String DESCRIPTION = "Logouts user from system";

    @Override
    public void execute() throws GeneralException {
        final User user = getAuthService().getUser();
        getAuthService().logout();
        System.out.println("User " + user.getLogin() + " logged out.");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
