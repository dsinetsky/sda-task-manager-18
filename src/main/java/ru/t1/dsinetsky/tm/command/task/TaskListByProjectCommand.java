package ru.t1.dsinetsky.tm.command.task;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectCommand extends AbstractTaskCommand {

    public static final String NAME = TerminalConst.CMD_LIST_TASKS_OF_PROJECT;

    public static final String DESCRIPTION = "Shows all tasks bound to project(if any)";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter Id of project:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> listTasks = getTaskService().returnTasksOfProject(projectId);
        showListTask(listTasks);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
