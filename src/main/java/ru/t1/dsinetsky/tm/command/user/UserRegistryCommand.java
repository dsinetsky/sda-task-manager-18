package ru.t1.dsinetsky.tm.command.user;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    public static final String NAME = TerminalConst.CMD_REGISTRY_USER;

    public static final String DESCRIPTION = "Registry user in system";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        final String password = TerminalUtil.nextLine();
        getAuthService().registry(login,password);
        System.out.println("User successfully registered!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
