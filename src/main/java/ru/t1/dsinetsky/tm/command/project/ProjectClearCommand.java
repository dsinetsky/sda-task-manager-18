package ru.t1.dsinetsky.tm.command.project;

import ru.t1.dsinetsky.tm.constant.TerminalConst;

public final class ProjectClearCommand extends AbstractProjectCommand {

    public static final String NAME = TerminalConst.CMD_PROJECT_CLEAR;

    public static final String DESCRIPTION = "Clear all projects";

    @Override
    public void execute() {
        getProjectService().clearAll();
        System.out.println("All projects successfully cleared!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
