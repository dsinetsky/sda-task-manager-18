package ru.t1.dsinetsky.tm.command.user;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    public static final String NAME = TerminalConst.CMD_SHOW_USER;

    public static final String DESCRIPTION = "Shows information about current user";

    @Override
    public void execute() throws GeneralException {
        final User user = getAuthService().getUser();
        System.out.println(user);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
