package ru.t1.dsinetsky.tm.command;

import ru.t1.dsinetsky.tm.api.model.ICommand;
import ru.t1.dsinetsky.tm.api.service.*;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract void execute() throws GeneralException;

    public abstract String getName();

    public abstract String getArgument();

    public abstract String getDescription();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString(){
        final String name = getName();
        final String argument = getArgument();
        final String desc = getDescription();
        String result = "";
        if (argument != null && !argument.isEmpty()) result += argument + ",";
        if (name != null && !name.isEmpty()) result += name + " - ";
        if (desc != null && !desc.isEmpty()) result += desc;
        return result;
    }

}
