package ru.t1.dsinetsky.tm.command.user;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class CurrentUserUpdateEmailCommand extends AbstractUserCommand {

    public static final String NAME = TerminalConst.CMD_UPDATE_CURRENT_USER_EMAIL;

    public static final String DESCRIPTION = "Updates email of current user";

    @Override
    public void execute() throws GeneralException {
        final String id = getAuthService().getUserId();
        System.out.println("Enter new email:");
        final String email = TerminalUtil.nextLine();
        getUserService().updateEmailById(id,email);
        System.out.println("User successfully updated!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
