package ru.t1.dsinetsky.tm.command.task;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public final class TaskClearCommand extends AbstractTaskCommand {

    public static final String NAME = TerminalConst.CMD_TASK_CREATE;

    public static final String DESCRIPTION = "Create new task";

    @Override
    public void execute() throws GeneralException {
        getTaskService().clearAll();
        System.out.println("Tasks successfully cleared!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
