package ru.t1.dsinetsky.tm.enumerated;

import ru.t1.dsinetsky.tm.exception.system.InvalidRoleException;

public enum Role {

    ADMIN("Administrator"),
    USUAL("User");

    private String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public static Role toRole(final String value) throws InvalidRoleException {
        if (value == null || value.isEmpty()) throw new InvalidRoleException();
        for (final Role role : values())
            if (value.equals(role.name())) return role;
        throw new InvalidRoleException();
    }

    public String getDisplayName() {
        return displayName;
    }

}
