package ru.t1.dsinetsky.tm.exception.user;

public final class UserPasswordIsEmptyException extends GeneralUserException {

    public UserPasswordIsEmptyException() {
        super("Password cannot be empty!");
    }

}
