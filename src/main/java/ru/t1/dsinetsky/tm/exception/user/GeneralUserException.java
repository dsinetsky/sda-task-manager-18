package ru.t1.dsinetsky.tm.exception.user;

import ru.t1.dsinetsky.tm.exception.GeneralException;

public abstract class GeneralUserException extends GeneralException {

    public GeneralUserException() {
    }

    public GeneralUserException(final String message) {
        super(message);
    }

    public GeneralUserException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GeneralUserException(final Throwable cause) {
        super(cause);
    }

    public GeneralUserException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
