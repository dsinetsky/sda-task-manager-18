package ru.t1.dsinetsky.tm.exception.user;

public final class UserAlreadyExistException extends GeneralUserException{

    public UserAlreadyExistException() {
        super("User already exists!");
    }

}
