package ru.t1.dsinetsky.tm.repository;

import ru.t1.dsinetsky.tm.api.repository.ICommandRepository;
import ru.t1.dsinetsky.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> mapByName = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> mapByArgument = new LinkedHashMap<>();

    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty()) mapByArgument.put(argument,command);
        final String commandName = command.getName();
        if (commandName != null && !commandName.isEmpty()) mapByName.put(commandName,command);
    }

    @Override
    public AbstractCommand getCommandByName(final String commandName) {
        if(commandName == null || commandName.isEmpty()) return null;
        return mapByName.get(commandName);
    }

    @Override
    public AbstractCommand getCommandByArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return mapByArgument.get(argument);
    }
}
