package ru.t1.dsinetsky.tm.repository;

import ru.t1.dsinetsky.tm.api.repository.IUserRepository;
import ru.t1.dsinetsky.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    public void add(final User user) {
        if(user == null) return;
        if (isUserIdExist(user.getId())) return;
        final String login = user.getLogin();
        if(login == null || login.isEmpty()) return;
        if (isUserLoginExist(login)) return;
        final String password = user.getPasswordHash();
        if(password == null || password.isEmpty()) return;
        users.add(user);
    }

    public List<User> returnAll() {
        return users;
    }

    public User findUserById(final String id) {
        if (id == null || id.isEmpty()) return null;
        for (final User search : users) {
            if (search.getId().equals(id)) return search;
        }
        return null;
    }

    public User findUserByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        for (final User search : users) {
            if (search.getLogin().equals(login)) return search;
        }
        return null;
    }

    public boolean isUserIdExist(final String id) {
        return findUserById(id) != null;
    }

    public boolean isUserLoginExist(final String login) {
        return findUserByLogin(login) != null;
    }

    public void removeById(final String id) {
        if (id == null || id.isEmpty()) return;
        final User user = findUserById(id);
        if (user == null) return;
        users.remove(user);
    }


    public void removeByLogin(final String login) {
        if (login == null || login.isEmpty()) return;
        final User user = findUserByLogin(login);
        if (user == null) return;
        users.remove(user);
    }

}
