package ru.t1.dsinetsky.tm.model;

import ru.t1.dsinetsky.tm.enumerated.Role;

import java.util.UUID;

public final class User {

    private String id = UUID.randomUUID().toString();

    private String passwordHash;

    private String login;

    private String email;

    private String firstName;

    private String lastName;

    private String middleName;

    private Role role = Role.USUAL;

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(final String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(final String middleName) {
        this.middleName = middleName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(final Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Id: ").append(id).append("\n")
                .append("Login: ").append(login).append(" - ").append(role.getDisplayName()).append("\n")
                .append("Name: ").append(lastName).append(" ").append(firstName).append(" ").append(middleName).append("\n")
                .append("Email: ").append(email).toString();
    }
}
