package ru.t1.dsinetsky.tm.api.repository;

import ru.t1.dsinetsky.tm.model.User;

import java.util.List;

public interface IUserRepository {

    void add(User user);

    User findUserById(String id);

    User findUserByLogin(String login);

    boolean isUserIdExist(String id);

    boolean isUserLoginExist(String login);

    void removeById(String id);

    void removeByLogin(String login);

    List<User> returnAll();

}
