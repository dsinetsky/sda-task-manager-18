package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.user.GeneralUserException;
import ru.t1.dsinetsky.tm.model.User;

import java.util.List;

public interface IUserService {

    User create(String login, String password) throws GeneralUserException;

    User create(String login, String password, Role role) throws GeneralUserException;

    User create(String login, String password, String email) throws GeneralUserException;

    User create(String login, String password, String firstName, String lastName, String middleName) throws GeneralUserException;

    User create(String login, String password, String email, String firstName, String lastName, String middleName) throws GeneralUserException;

    User create(String login, String password, Role role, String email, String firstName, String lastName, String middleName) throws GeneralUserException;

    User create(User newUser) throws GeneralUserException;

    void removeUserById(String id) throws GeneralUserException;

    void removeUserByLogin(String login) throws GeneralUserException;

    User findUserById(String id) throws GeneralUserException;

    User findUserByLogin(String login) throws GeneralUserException;

    User updateUserById(String id, String firstName, String lastName, String middleName) throws GeneralUserException;

    User updateUserByLogin(String login, String firstName, String lastName, String middleName) throws GeneralUserException;

    User updateEmailById(String id, String email) throws GeneralUserException;

    User updateEmailByLogin(String login, String email) throws GeneralUserException;

    User changePassword(String id, String password) throws GeneralUserException;

    List<User> returnAll();

    boolean isUserExistById(String id) throws GeneralUserException;

    boolean isUserExistByLogin(String login) throws GeneralUserException;

}
