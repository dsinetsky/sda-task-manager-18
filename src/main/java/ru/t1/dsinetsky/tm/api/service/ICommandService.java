package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.command.AbstractCommand;
import ru.t1.dsinetsky.tm.exception.system.InvalidArgumentException;
import ru.t1.dsinetsky.tm.exception.system.InvalidCommandException;

import java.util.Collection;

public interface ICommandService {

    Collection<AbstractCommand> getTerminalCommands();

    void add(AbstractCommand command);

    AbstractCommand getCommandByName(String commandName) throws InvalidCommandException;

    AbstractCommand getCommandByArgument(String argument) throws InvalidArgumentException;

}
